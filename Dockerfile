FROM node

ENV BW_CLI_VERSION=2024.10.0

RUN npm i -g @bitwarden/cli

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
